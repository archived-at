/*
 * Archived-At Thunderbird Add-On
 *
 * Copyright (c) Jan Kiszka, 2015
 *
 * Authors:
 *  Jan Kiszka <jan.kiszka@web.de>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* Activation */
pref("extensions.archived-at.add_to_context_menu", true);
pref("extensions.archived-at.add_to_other_actions", true);

/* Post-processing */
pref("extensions.archived-at.post_processing_rules",
     '[{"search":"","replace":""}]');
