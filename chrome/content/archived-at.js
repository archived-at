/*
 * Archived-At Thunderbird Add-On
 *
 * Copyright (c) Jan Kiszka, 2015, 2016
 *
 * Authors:
 *  Jan Kiszka <jan.kiszka@web.de>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

Components.utils.import("resource://gre/modules/devtools/Console.jsm");

var archivedAt = {
    ScriptableInputStream:
        Components.Constructor("@mozilla.org/scriptableinputstream;1",
                               "nsIScriptableInputStream", "init"),
    messengerInstance: Components.classes["@mozilla.org/messenger;1"]
        .createInstance(Components.interfaces.nsIMessenger),
    clipboardHelper:
        Components.classes["@mozilla.org/widget/clipboardhelper;1"]
            .getService(Components.interfaces.nsIClipboardHelper),

    contextMenuItem: null,
    actionMenuItem: null,
    archivedAtURLs: new Array(),
    prefs: null,
};

var archivedAtHeaderListener = {
    header: "",
    done: false,

    onDataAvailable: function(request, context, inputStream, offset, count)
    {
        var scriptStream = new archivedAt.ScriptableInputStream(inputStream);

        if (!this.done) {
            var splitInput = scriptStream.read(count)
                    .replace(/\r/g, "").split("\n\n");

            this.header = this.header + splitInput[0];
            if (splitInput.length > 1)
                this.done = true;
        } else {
            scriptStream.read(count);
        }
        scriptStream.close();
    },

    onStartRequest: function(request, context)
    {
        this.header = "";
        this.done = false;
    },

    onStopRequest: function(request, context, code)
    {
        while (true) {
            const tag = "\nArchived-At: <";
            var start = this.header.indexOf(tag);
            if (start < 0)
                break;

            var url = this.header.substr(start + tag.length);
            var end = url.indexOf(">");
            if (end < 0)
                break;
            archivedAt.archivedAtURLs.push(url.substr(0, end))
            console.log(archivedAt.archivedAtURLs);

            this.header = this.header.substr(start + tag.length + end);
        }
        archivedAt.updateMenuItems();
    },
};

var archivedAtMsgListener = {
    onStartHeaders: function()
    {
    },

    onEndHeaders: function()
    {
        var msg = gFolderDisplay.selectedMessage;
        var msgURI = msg.folder.getUriForMsg(msg);
        var service =
            archivedAt.messengerInstance.messageServiceFromURI(msgURI);

        archivedAt.archivedAtURLs = new Array();
        try {
            service.CopyMessage(msgURI, archivedAtHeaderListener, false, null,
                                msgWindow, {});
        }
        catch (e) {
            archivedAt.updateMenuItems();
        }
    },

    beforeStartHeaders: function()
    {
        return true;
    }
};

archivedAt.copyToClipboard = function()
{
    if (this.archivedAtURLs.length > 0) {
        var url = this.archivedAtURLs[0];

        try {
            var prefString = this.prefs.getCharPref("post_processing_rules");
            var rules = JSON.parse(prefString);

            for (var i = 0; i < rules.length; i++)
                url = url.replace(rules[i].search, rules[i].replace);
        }
        catch (e) {}

        this.clipboardHelper.copyString(url);
        console.log("copied: " + url);
    }
}

archivedAt.updateMenuItems = function()
{
    archivedAt.contextMenuItem.hidden =
        (!this.prefs.getBoolPref("add_to_context_menu") ||
         this.archivedAtURLs.length == 0);
    archivedAt.actionMenuItem.hidden =
        (!this.prefs.getBoolPref("add_to_other_actions") ||
         this.archivedAtURLs.length == 0);
}

archivedAt.observe = function(subject, topic, data)
{
    if (topic == "nsPref:changed")
        this.updateMenuItems()
}

archivedAt.setup = function()
{
    this.contextMenuItem =
        document.getElementById("archived-at.mailContext.copyToClipboard");
    this.actionMenuItem =
        document.getElementById("archived-at.otherAction.copyToClipboard");

    this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
        .getService(Components.interfaces.nsIPrefService)
        .getBranch("extensions.archived-at.");
    this.prefs.addObserver("", archivedAt, false);

    gMessageListeners.push(archivedAtMsgListener);
}

addEventListener("messagepane-loaded",
                 function() { archivedAt.setup(); }, true);
