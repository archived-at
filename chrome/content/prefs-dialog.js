/*
 * Archived-At Thunderbird Add-On
 *
 * Copyright (c) Jan Kiszka, 2015
 *
 * Authors:
 *  Jan Kiszka <jan.kiszka@web.de>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

Components.utils.import("resource://gre/modules/devtools/Console.jsm");

var archivedAtPrefs = {
    prefs: null,
    rules: null,
    searchFor: null,
    replaceWith: null,
}

archivedAtPrefs.onLoad = function()
{
    this.prefs = Components.classes["@mozilla.org/preferences-service;1"]
        .getService(Components.interfaces.nsIPrefService)
        .getBranch("extensions.archived-at.");

    this.searchFor = document.getElementById("archived-at.searchFor");
    this.replaceWith = document.getElementById("archived-at.replaceWith");

    try {
        var prefString = this.prefs.getCharPref("post_processing_rules");
        this.rules = JSON.parse(prefString);

        this.searchFor.value = this.rules[0].search;
        this.replaceWith.value = this.rules[0].replace;
    }
    catch (e) {
        this.rules = new Array({search:"", replace: ""});
    }
}

archivedAtPrefs.onUnload = function()
{
    this.rules[0].search = this.searchFor.value;
    this.rules[0].replace = this.replaceWith.value;

    this.prefs.setCharPref("post_processing_rules",
                           JSON.stringify(this.rules));
}
